FROM node:21

WORKDIR /app

COPY package.json .
COPY package-lock.json .
COPY . .

RUN npm install

RUN npm run build

ENTRYPOINT ["npm", "run", "start-prod"]
